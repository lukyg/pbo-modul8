package No_4;

import java.util.Scanner;

class PanggilMenuUtama extends PengaturanMenu{

    @Override
    protected void MenuUtama(String pilihan) {
        if(pilihan.equals("1")){
            System.out.println("Ini menu ke satu");
        }
        else if(pilihan.equals("2")){
            System.out.println("Ini menu ke dua");
        }
        else{
            System.out.println("Pilihan salah geng");
        }
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        PanggilMenuUtama panggilMenuUtama = new PanggilMenuUtama();
        System.out.print("Masukkan pilihan : ");
        String pilihan = input.nextLine();
        panggilMenuUtama.MenuUtama(pilihan);

        input.close();
    }
}
