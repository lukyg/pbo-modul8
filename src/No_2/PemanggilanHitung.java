package No_2;

import java.util.Scanner;

class PemanggilanHitung extends Perhitungan{

    @Override
    public void Segitiga(double alas, double tinggi) {
        double hasil, nilaiAlas, nilaiTinggi;
        nilaiAlas = alas;
        nilaiTinggi = tinggi;
        hasil = nilaiAlas * nilaiTinggi * 0.5;
        System.out.println("Hasil hitung adalah "+hasil);
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        double nilaiAlas, nilaiTinggi;
        System.out.print("Masukkan nilai alas : ");
        nilaiAlas = input.nextDouble();
        System.out.print("Masukkan nilai tinggi : ");
        nilaiTinggi = input.nextDouble();

        PemanggilanHitung pemanggilanHitung = new PemanggilanHitung();
        pemanggilanHitung.Segitiga(nilaiAlas,nilaiTinggi);
    }
}
