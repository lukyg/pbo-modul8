package No_8;

public class CallBinatang {
    public static void main(String[] args) {
        Binatang binatang = new Binatang();
        System.out.println("Ini untuk panggil semua");
        binatang.Katak();
        binatang.Kodok();
        binatang.Kuda();
        binatang.Sapi();

        Binatang binatang1 = new Binatang();
        System.out.println("Ini panggil salah satu dari masing-masing method");
        binatang1.Kodok();
        binatang1.Kuda();

    }
}
