package No_8;

public class Binatang implements Amfibi, Mamalia {
    @Override
    public void Kodok() {
        System.out.println("Ini kodok");
    }

    @Override
    public void Katak() {
        System.out.println("Ini katak");
    }

    @Override
    public void Kuda() {
        System.out.println("Ini kuda");
    }

    @Override
    public void Sapi() {
        System.out.println("Ini sapi");
    }
}
