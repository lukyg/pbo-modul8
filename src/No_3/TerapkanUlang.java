package No_3;

import java.util.Scanner;

class TerapkanUlang extends CobaUlang{

    @Override
    public void testUlang(int banyak) {
        for(int i = 0; i<banyak;i++){
            System.out.println("Ulang terus!");
        }
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        TerapkanUlang terapkanUlang = new TerapkanUlang();
        System.out.print("Masukkan jumlah perulangan : ");
        int banyak = input.nextInt();
        terapkanUlang.testUlang(banyak);

    }
}
