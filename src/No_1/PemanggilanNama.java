package No_1;

import java.util.Scanner;

class PemanggilanNama extends Identitas{

    @Override
    public void testNama() {
        Scanner input = new Scanner(System.in);
        System.out.print("Masukkan nama :");
        String nama = input.nextLine();
        System.out.println("Nama saya adalah "+nama);
    }

    public static void main(String[] args) {
        PemanggilanNama pemanggilanNama = new PemanggilanNama();
        pemanggilanNama.testNama();
    }
}
